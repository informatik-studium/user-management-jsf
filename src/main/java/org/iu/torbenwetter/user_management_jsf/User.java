package org.iu.torbenwetter.user_management_jsf;

public class User {

    private String surname;
    private String lastname;

    public User(String surname, String lastname) {
        this.surname = surname;
        this.lastname = lastname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    @Override
    public String toString() {
        return "org.iu.torbenwetter.user_management_jsf.User{surname='" + surname + "', lastname='" + lastname + "'}";
    }
}
