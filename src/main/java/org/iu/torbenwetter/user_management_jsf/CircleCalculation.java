package org.iu.torbenwetter.user_management_jsf;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "circle_calculation")
public class CircleCalculation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "radius", nullable = false)
    private double radius;

    @Column(name = "area", nullable = false)
    private double area;

    @Column(name = "timestamp", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public CircleCalculation(double radius, double area) {
        this.radius = radius;
        this.area = area;
    }

    /**
     * @deprecated Default constructor for JPA.
     */
    public CircleCalculation() {
    }

    public long getId() {
        return id;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
