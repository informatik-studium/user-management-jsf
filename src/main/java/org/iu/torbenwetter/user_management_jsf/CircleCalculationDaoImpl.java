package org.iu.torbenwetter.user_management_jsf;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CircleCalculationDaoImpl implements CircleCalculationDao {

    @Override
    public void save(CircleCalculation circleCalculation) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.persist(circleCalculation);
        session.close();
    }

    @Override
    public List<CircleCalculation> getAll() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        CriteriaQuery<CircleCalculation> criteriaQuery = session.getCriteriaBuilder().createQuery(CircleCalculation.class);
        List<CircleCalculation> circleCalculations = session.createQuery(criteriaQuery.select(criteriaQuery.from(CircleCalculation.class))).getResultList();
        session.close();
        return circleCalculations;
    }
}
