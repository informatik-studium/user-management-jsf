package org.iu.torbenwetter.user_management_jsf;

import java.util.List;

public interface CircleCalculationDao {
    void save(CircleCalculation circleCalculation);

    List<CircleCalculation> getAll();
}
