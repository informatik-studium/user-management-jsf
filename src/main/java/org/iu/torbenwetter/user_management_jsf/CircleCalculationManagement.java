package org.iu.torbenwetter.user_management_jsf;

import java.util.List;

public class CircleCalculationManagement {

    private static final CircleCalculationDao circleCalculationDao = new CircleCalculationDaoImpl();

    public static void addCircleCalculation(CircleCalculation circleCalculation) {
        circleCalculationDao.save(circleCalculation);
    }

    public static List<CircleCalculation> getAllCircleCalculations() {
        return circleCalculationDao.getAll();
    }
}
