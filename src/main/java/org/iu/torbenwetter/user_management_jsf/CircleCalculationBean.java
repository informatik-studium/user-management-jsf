package org.iu.torbenwetter.user_management_jsf;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class CircleCalculationBean implements Serializable {

    private double radius;
    private double area;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return area;
    }

    public void calculateArea() {
        area = Math.PI * Math.pow(radius, 2);
        CircleCalculationManagement.addCircleCalculation(new CircleCalculation(radius, area));
    }
}
