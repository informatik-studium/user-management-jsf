package org.iu.torbenwetter.user_management_jsf;


import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class UserManagementBean implements Serializable {

    private String surname;
    private String lastname;

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public List<User> getUsers() {
        return UserManagement.getUsers();
    }

    public String addUser() {
        UserManagement.addUser(new User(surname, lastname));
        return "index";
    }
}
