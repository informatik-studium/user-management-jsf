package org.iu.torbenwetter.user_management_jsf;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Properties properties = new Properties();
            properties.put(Environment.DRIVER, "org.postgresql.Driver");
            properties.put(Environment.URL, "jdbc:postgresql://localhost:60686/user_management");
            properties.put(Environment.USER, "db");
            properties.put(Environment.PASS, "db");
            properties.put(Environment.POOL_SIZE, "10");
            properties.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
            properties.put(Environment.HBM2DDL_AUTO, "update");
            Configuration configuration = new Configuration();
            Configuration configuration1 = configuration.setProperties(properties);
            Configuration configuration2 = configuration1.addAnnotatedClass(CircleCalculation.class);
            sessionFactory = configuration2.buildSessionFactory();
        }
        return sessionFactory;
    }
}
