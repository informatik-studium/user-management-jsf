package org.iu.torbenwetter.user_management_jsf;

import java.util.ArrayList;
import java.util.List;

public class UserManagement {

    private static final List<User> users = new ArrayList<>();

    private UserManagement() {
    }

    public static List<User> getUsers() {
        return users;
    }

    public static void addUser(User user) {
        users.add(user);
    }
}
